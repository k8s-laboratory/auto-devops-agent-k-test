<!--
 SPDX-FileCopyrightText: 2023 Idiap Research Institute <contact@idiap.ch>
 SPDX-FileContributor: Samuel Gaist <samuel.gaist@idiap.ch>

 SPDX-License-Identifier: MIT
-->

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://spdx.org/licenses/MIT.html)

# Simple Python API

Example of REST service built on top of FastAPI

## Development setup

To run (in isolation), either:

Run from active Python environment using `uvicorn`:

    pip install -r requirements.txt
    uvicorn service.main:app --host 0.0.0.0 --port 8080 --reload

Navigate to http://localhost:8080/docs to test the API.

The API responds to the ping endpoint
