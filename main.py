# SPDX-FileCopyrightText: 2023 Idiap Research Institute <contact@idiap.ch>
# SPDX-FileContributor: Samuel Gaist <samuel.gaist@idiap.ch>
#
# SPDX-License-Identifier: MIT

from fastapi import APIRouter
from fastapi import FastAPI

root_router = APIRouter()


@root_router.get("/ping", summary="Check that the service is operational")
def pong():
    """
    Sanity check - this will let the user know that the service is operational.

    It is also used as part of the HEALTHCHECK. Docker uses curl to check that
    the API service is still running, by exercising this endpoint.

    """
    return {"ping": "pong"}


@root_router.get("/", summary="Default answer")
def root():
    """
    If nothing is returned here the service will be considered as non working
    since it would return 404

    """
    return {"Ahoy": "v0.0.1"}


app = FastAPI(title="Example")
app.include_router(root_router)
